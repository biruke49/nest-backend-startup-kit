import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { News } from '@publication/domains/news';
import { INewsRepository } from '@publication/domains/news.repository.interface';
import { Repository } from 'typeorm';
import { NewsEntity } from './news.entity';

@Injectable()
export class NewsRepository implements INewsRepository {
  constructor(
    @InjectRepository(NewsEntity)
    private newsRepository: Repository<NewsEntity>,
  ) {}
  async insert(news: News): Promise<News> {
    const newsEntity = this.toNewsEntity(news);
    const result = await this.newsRepository.save(newsEntity);
    return result ? this.toNews(result) : null;
  }
  async update(news: News): Promise<News> {
    const newsEntity = this.toNewsEntity(news);
    const result = await this.newsRepository.save(newsEntity);
    return result ? this.toNews(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.newsRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.newsRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.newsRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getAllNews(withDeleted: boolean): Promise<News[]> {
    const allNews = await this.newsRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!allNews.length) {
      return null;
    }
    return allNews.map((news) => this.toNews(news));
  }
  async getNewsById(id: string, withDeleted = false): Promise<News> {
    const news = await this.newsRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!news[0]) {
      return null;
    }
    return this.toNews(news[0]);
  }
  toNews(newsEntity: NewsEntity): News {
    const news = new News();
    news.id = newsEntity.id;
    news.title = newsEntity.title;
    news.description = newsEntity.description;
    news.coverImage = newsEntity.coverImage;
    news.archiveReason = newsEntity.archiveReason;
    news.createdBy = newsEntity.createdBy;
    news.updatedBy = newsEntity.updatedBy;
    news.deletedBy = newsEntity.deletedBy;
    news.createdAt = newsEntity.createdAt;
    news.updatedAt = newsEntity.updatedAt;
    news.deletedAt = newsEntity.deletedAt;
    return news;
  }
  toNewsEntity(news: News): NewsEntity {
    const newsEntity = new NewsEntity();
    newsEntity.id = news.id;
    newsEntity.title = news.title;
    newsEntity.description = news.description;
    newsEntity.coverImage = news.coverImage;
    newsEntity.archiveReason = news.archiveReason;
    newsEntity.createdBy = news.createdBy;
    newsEntity.updatedBy = news.updatedBy;
    newsEntity.deletedBy = news.deletedBy;
    newsEntity.createdAt = news.createdAt;
    newsEntity.updatedAt = news.updatedAt;
    newsEntity.deletedAt = news.deletedAt;
    return newsEntity;
  }
}
