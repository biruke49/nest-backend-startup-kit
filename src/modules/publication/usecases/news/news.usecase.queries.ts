import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NewsEntity } from '@publication/persistence/news/news.entity';
import { Repository } from 'typeorm';
import { NewsResponse } from './news.response';

@Injectable()
export class NewsQuery {
  constructor(
    @InjectRepository(NewsEntity)
    private newsRepository: Repository<NewsEntity>,
  ) {}
  async getAllNews(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<NewsResponse>> {
    const dataQuery = QueryConstructor.constructQuery<NewsEntity>(
      this.newsRepository,
      query,
    );
    const d = new DataResponseFormat<NewsResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => NewsResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedNewsById(
    id: string,
    withDeleted = false,
  ): Promise<NewsResponse> {
    const news = await this.newsRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!news[0]) {
      throw new NotFoundException(`News not found with id ${id}`);
    }
    return NewsResponse.fromEntity(news[0]);
  }
  async getNewsById(id: string, withDeleted = false): Promise<NewsResponse> {
    const news = await this.newsRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!news[0]) {
      throw new NotFoundException(`News not found with id ${id}`);
    }
    return NewsResponse.fromEntity(news[0]);
  }
  async getArchivedNews(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<NewsResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<NewsEntity>(
      this.newsRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<NewsResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => NewsResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
