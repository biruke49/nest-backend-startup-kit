import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { News } from '@publication/domains/news';
import { NewsEntity } from '@publication/persistence/news/news.entity';

export class NewsResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  coverImage: FileDto;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(newsEntity: NewsEntity): NewsResponse {
    const newsResponse = new NewsResponse();
    newsResponse.id = newsEntity.id;
    newsResponse.title = newsEntity.title;
    newsResponse.description = newsEntity.description;
    newsResponse.coverImage = newsEntity.coverImage;
    newsResponse.archiveReason = newsEntity.archiveReason;
    newsResponse.createdBy = newsEntity.createdBy;
    newsResponse.updatedBy = newsEntity.updatedBy;
    newsResponse.deletedBy = newsEntity.deletedBy;
    newsResponse.createdAt = newsEntity.createdAt;
    newsResponse.updatedAt = newsEntity.updatedAt;
    newsResponse.deletedAt = newsEntity.deletedAt;
    return newsResponse;
  }
  static fromDomain(news: News): NewsResponse {
    const newsResponse = new NewsResponse();
    newsResponse.id = news.id;
    newsResponse.title = news.title;
    newsResponse.description = news.description;
    newsResponse.coverImage = news.coverImage;
    newsResponse.archiveReason = news.archiveReason;
    newsResponse.createdBy = news.createdBy;
    newsResponse.updatedBy = news.updatedBy;
    newsResponse.deletedBy = news.deletedBy;
    newsResponse.createdAt = news.createdAt;
    newsResponse.updatedAt = news.updatedAt;
    newsResponse.deletedAt = news.deletedAt;
    return newsResponse;
  }
}
