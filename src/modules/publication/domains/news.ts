import { FileDto } from '@libs/common/file-dto';

export class News {
  id: string;
  title: string;
  description: string;
  coverImage?: FileDto;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
