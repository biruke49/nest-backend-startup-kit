import { UserInfo } from '@account/dtos/user-info.dto';
import { CredentialType } from '@libs/common/enums';
import { CanActivate, ExecutionContext, Type } from '@nestjs/common';

export function RolesGuard(roles: string): Type<CanActivate> {
  class RolesGuardMixin implements CanActivate {
    canActivate(context: ExecutionContext) {
      const requiredRoles = roles.split('|');
      if (requiredRoles.length < 1) {
        return true;
      }

      const request = context.switchToHttp().getRequest();
      const user: UserInfo = request.user;
      if (
        (user.type === CredentialType.Passenger &&
          requiredRoles.includes(CredentialType.Passenger)) ||
        (user.type === CredentialType.Driver &&
          requiredRoles.includes(CredentialType.Driver)) ||
        (user.type === CredentialType.Parent &&
          requiredRoles.includes(CredentialType.Parent)) ||
        (user.type === CredentialType.School &&
          requiredRoles.includes(CredentialType.School)) ||
        (user.type === CredentialType.Corporate &&
          requiredRoles.includes(CredentialType.Corporate)) ||
        (user.type === CredentialType.Owner &&
          requiredRoles.includes(CredentialType.Owner))
      ) {
        return true;
      } else if (user && user.role) {
        const userRoles = user.role;
        return requiredRoles.some(
          (requiredRole) =>
            userRoles?.key === requiredRole.trim() ||
            userRoles?.key === 'super_admin',
        );
      }
      return false;
    }
  }

  return RolesGuardMixin;
}
