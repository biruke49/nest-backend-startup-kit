import { Address } from '@libs/common/address';
import { CommonEntity } from '@libs/common/common.entity';
import { FileDto } from '@libs/common/file-dto';
import { Column, Entity, Index, OneToMany, PrimaryColumn } from 'typeorm';
import { AccountPermissionEntity } from './account-permission.entity';
import { AccountRoleEntity } from './account-role.entity';
import { NotificationEntity } from '@notification/persistence/notifications/notification.entity';

@Entity('accounts')
export class AccountEntity extends CommonEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Index()
  @Column({ name: 'phone_number' })
  phoneNumber: string;
  @Column({ nullable: true })
  email: string;
  @Column()
  type: string;
  @Column({ nullable: true })
  gender: string;
  @Index()
  @Column({ nullable: true })
  username: string;
  @Column({ name: 'is_active' })
  isActive: boolean;
  @Column()
  password: string;
  @Column({ nullable: true, name: 'fcm_id' })
  fcmId: string;
  @Column({ name: 'address', type: 'jsonb', nullable: true })
  address: Address;
  @Column({ name: 'profile_image', type: 'jsonb', nullable: true })
  profileImage: FileDto;
  @OneToMany(() => AccountRoleEntity, (accountRole) => accountRole.account, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  public accountRoles: AccountRoleEntity[];
  @OneToMany(
    () => AccountPermissionEntity,
    (accountPermission) => accountPermission.account,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  public accountPermissions: AccountPermissionEntity[];
  @OneToMany(() => NotificationEntity, (chat) => chat.accountReceiver, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  notificationReceiver: NotificationEntity[];
}
