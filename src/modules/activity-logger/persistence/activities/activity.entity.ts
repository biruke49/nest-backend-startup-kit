import { CommonEntity } from '@libs/common/common.entity';
import { UserInfo } from '@account/dtos/user-info.dto';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity('activities')
export class ActivityEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'model_id', type: 'uuid' })
  modelId: string;
  @Column({
    name: 'model_name',
    nullable: true,
  })
  modelName: string;
  @Column({ name: 'user_id', type: 'uuid' })
  userId: string;
  @Column({
    name: 'action',
    nullable: true,
  })
  action: string;
  @Column({ nullable: true })
  ip?: string;
  @Column({ nullable: true, name: 'old_payload', type: 'jsonb' })
  oldPayload?: any;
  @Column({ nullable: true, name: 'payload', type: 'jsonb' })
  payload?: any;
  @Column({ nullable: true, name: 'user', type: 'jsonb' })
  user: UserInfo;
}
