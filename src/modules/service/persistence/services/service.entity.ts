import { CommonEntity } from '@libs/common/common.entity';
import { FileDto } from '@libs/common/file-dto';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('services')
export class ServiceEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column({ type: 'text' })
  description: string;
  @Column({ type: 'jsonb', nullable: true, name: 'cover_image' })
  coverImage: FileDto;
}
