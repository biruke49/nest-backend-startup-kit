import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Service } from '@service/domains/services/service';
import { IServiceRepository } from '@service/domains/services/service.repository.interface';
import { Repository } from 'typeorm';
import { ServiceEntity } from './service.entity';

@Injectable()
export class ServiceRepository implements IServiceRepository {
  constructor(
    @InjectRepository(ServiceEntity)
    private serviceRepository: Repository<ServiceEntity>,
  ) {}
  async insert(service: Service): Promise<Service> {
    const serviceEntity = this.toServiceEntity(service);
    const result = await this.serviceRepository.save(serviceEntity);
    return result ? this.toService(result) : null;
  }
  async update(service: Service): Promise<Service> {
    const serviceEntity = this.toServiceEntity(service);
    const result = await this.serviceRepository.save(serviceEntity);
    return result ? this.toService(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.serviceRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.serviceRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.serviceRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getServices(withDeleted: boolean): Promise<Service[]> {
    const allServices = await this.serviceRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!allServices.length) {
      return null;
    }
    return allServices.map((service) => this.toService(service));
  }
  async getServiceById(id: string, withDeleted = false): Promise<Service> {
    const service = await this.serviceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!service[0]) {
      return null;
    }
    return this.toService(service[0]);
  }
  toService(serviceEntity: ServiceEntity): Service {
    const service = new Service();
    service.id = serviceEntity.id;
    service.title = serviceEntity.title;
    service.description = serviceEntity.description;
    service.coverImage = serviceEntity.coverImage;
    service.archiveReason = serviceEntity.archiveReason;
    service.createdBy = serviceEntity.createdBy;
    service.updatedBy = serviceEntity.updatedBy;
    service.deletedBy = serviceEntity.deletedBy;
    service.createdAt = serviceEntity.createdAt;
    service.updatedAt = serviceEntity.updatedAt;
    service.deletedAt = serviceEntity.deletedAt;
    return service;
  }
  toServiceEntity(service: Service): ServiceEntity {
    const serviceEntity = new ServiceEntity();
    serviceEntity.id = service.id;
    serviceEntity.title = service.title;
    serviceEntity.description = service.description;
    serviceEntity.coverImage = service.coverImage;
    serviceEntity.archiveReason = service.archiveReason;
    serviceEntity.createdBy = service.createdBy;
    serviceEntity.updatedBy = service.updatedBy;
    serviceEntity.deletedBy = service.deletedBy;
    serviceEntity.createdAt = service.createdAt;
    serviceEntity.updatedAt = service.updatedAt;
    serviceEntity.deletedAt = service.deletedAt;
    return serviceEntity;
  }
}
