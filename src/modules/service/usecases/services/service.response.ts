import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { Service } from '@service/domains/services/service';
import { ServiceEntity } from '@service/persistence/services/service.entity';

export class ServiceResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  coverImage: FileDto;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(serviceEntity: ServiceEntity): ServiceResponse {
    const serviceResponse = new ServiceResponse();
    serviceResponse.id = serviceEntity.id;
    serviceResponse.title = serviceEntity.title;
    serviceResponse.description = serviceEntity.description;
    serviceResponse.coverImage = serviceEntity.coverImage;
    serviceResponse.archiveReason = serviceEntity.archiveReason;
    serviceResponse.createdBy = serviceEntity.createdBy;
    serviceResponse.updatedBy = serviceEntity.updatedBy;
    serviceResponse.deletedBy = serviceEntity.deletedBy;
    serviceResponse.createdAt = serviceEntity.createdAt;
    serviceResponse.updatedAt = serviceEntity.updatedAt;
    serviceResponse.deletedAt = serviceEntity.deletedAt;
    return serviceResponse;
  }
  static fromDomain(service: Service): ServiceResponse {
    const serviceResponse = new ServiceResponse();
    serviceResponse.id = service.id;
    serviceResponse.title = service.title;
    serviceResponse.description = service.description;
    serviceResponse.coverImage = service.coverImage;
    serviceResponse.archiveReason = service.archiveReason;
    serviceResponse.createdBy = service.createdBy;
    serviceResponse.updatedBy = service.updatedBy;
    serviceResponse.deletedBy = service.deletedBy;
    serviceResponse.createdAt = service.createdAt;
    serviceResponse.updatedAt = service.updatedAt;
    serviceResponse.deletedAt = service.deletedAt;
    return serviceResponse;
  }
}
