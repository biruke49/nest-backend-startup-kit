import { Configuration } from '@configurations/domains/configuration/configuration';
import { IConfigurationRepository } from '@configurations/domains/configuration/configuration.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigurationEntity } from './configuration.entity';

@Injectable()
export class ConfigurationRepository implements IConfigurationRepository {
  constructor(
    @InjectRepository(ConfigurationEntity)
    private configurationRepository: Repository<ConfigurationEntity>,
  ) {}
  async insert(configuration: Configuration): Promise<Configuration> {
    const configurationEntity = this.toConfigurationEntity(configuration);
    const result = await this.configurationRepository.save(configurationEntity);
    return result ? this.toConfiguration(result) : null;
  }
  async update(configuration: Configuration): Promise<Configuration> {
    const configurationEntity = this.toConfigurationEntity(configuration);
    const result = await this.configurationRepository.save(configurationEntity);
    return result ? this.toConfiguration(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.configurationRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Configuration[]> {
    const configurations = await this.configurationRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!configurations.length) {
      return null;
    }
    return configurations.map((user) => this.toConfiguration(user));
  }
  async getById(id: string, withDeleted = false): Promise<Configuration> {
    const configuration = await this.configurationRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!configuration[0]) {
      return null;
    }
    return this.toConfiguration(configuration[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.configurationRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.configurationRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toConfiguration(configurationEntity: ConfigurationEntity): Configuration {
    const configuration = new Configuration();
    configuration.id = configurationEntity.id;
    configuration.globalConfigurations =
      configurationEntity.globalConfigurations;
    configuration.archiveReason = configurationEntity.archiveReason;
    configuration.createdBy = configurationEntity.createdBy;
    configuration.updatedBy = configurationEntity.updatedBy;
    configuration.deletedBy = configurationEntity.deletedBy;
    configuration.createdAt = configurationEntity.createdAt;
    configuration.updatedAt = configurationEntity.updatedAt;
    configuration.deletedAt = configurationEntity.deletedAt;
    return configuration;
  }
  toConfigurationEntity(configuration: Configuration): ConfigurationEntity {
    const configurationEntity = new ConfigurationEntity();
    configurationEntity.id = configuration.id;
    configurationEntity.globalConfigurations =
      configuration.globalConfigurations;
    configurationEntity.archiveReason = configuration.archiveReason;
    configurationEntity.createdBy = configuration.createdBy;
    configurationEntity.updatedBy = configuration.updatedBy;
    configurationEntity.deletedBy = configuration.deletedBy;
    configurationEntity.createdAt = configuration.createdAt;
    configurationEntity.updatedAt = configuration.updatedAt;
    configurationEntity.deletedAt = configuration.deletedAt;
    return configurationEntity;
  }
}
