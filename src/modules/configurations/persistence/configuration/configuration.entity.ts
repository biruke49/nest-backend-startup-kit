import { GlobalConfigurations } from '@configurations/usecases/configuration/configuration.commands';
import { CommonEntity } from '@libs/common/common.entity';
import { Column, Double, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('configurations')
export class ConfigurationEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'jsonb', name: 'global_configurations' })
  globalConfigurations: GlobalConfigurations;
}
