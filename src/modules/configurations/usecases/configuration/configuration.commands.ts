import { UserInfo } from '@account/dtos/user-info.dto';
import { Configuration } from '@configurations/domains/configuration/configuration';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class GlobalConfigurations {
  @ApiProperty()
  @IsNotEmpty()
  timeout: number = 30;
  @ApiProperty()
  @IsNotEmpty()
  isBeingMaintained: boolean = false;
  @ApiProperty()
  @IsNotEmpty()
  telebirrStatus: boolean = true;
  @ApiProperty()
  @IsNotEmpty()
  chapaStatus: boolean = true;
}

export class CreateConfigurationCommand {
  @ApiProperty()
  @IsNotEmpty()
  globalConfigurations: GlobalConfigurations;
  currentUser: UserInfo;
  static fromCommand(command: CreateConfigurationCommand): Configuration {
    const configuration = new Configuration();
    configuration.globalConfigurations = command.globalConfigurations;
    return configuration;
  }
}
export class UpdateConfigurationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  globalConfigurations: GlobalConfigurations;
  currentUser: UserInfo;
  static fromCommand(command: UpdateConfigurationCommand): Configuration {
    const configuration = new Configuration();
    configuration.id = command.id;
    configuration.globalConfigurations = command.globalConfigurations;
    return configuration;
  }
}
export class ArchiveConfigurationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
