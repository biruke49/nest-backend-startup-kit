import { NotificationCommands } from './usecases/notifications/notification.usecase.commands';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { NotificationEntity } from './persistence/notifications/notification.entity';
import { NotificationRepository } from './persistence/notifications/notification.repository';
import { NotificationQuery } from './usecases/notifications/notification.usecase.queries';
import { NotificationsController } from './controllers/notification.controller';
import { AppService } from 'app.service';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
@Module({
  imports: [
    TypeOrmModule.forFeature([
      NotificationEntity,
      AccountEntity
    ]),
  ],
  providers: [
    NotificationCommands,
    NotificationRepository,
    NotificationQuery,
    AppService,
    AccountRepository,
  ],
  controllers: [NotificationsController],
})
export class NotificationModule {}
