import { Notification } from '@notification/domains/notifications/notification';
import {
  ArchiveNotificationCommand,
  CreateNotificationCommand,
  UpdateNotificationCommand,
} from './notification.commands';
import { NotificationRepository } from '@notification/persistence/notifications/notification.repository';
import { NotificationResponse } from './notification.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { AppService } from 'app.service';
import { NotificationQuery } from './notification.usecase.queries';
import { CredentialType } from '@libs/common/enums';
@Injectable()
export class NotificationCommands {
  constructor(
    private notificationRepository: NotificationRepository,
    private readonly eventEmitter: EventEmitter2,
    private accountRepository: AccountRepository,
    private appService: AppService,
    private notificationQuery: NotificationQuery,
  ) {}
  @OnEvent('create.notificationn')
  async createNotification(command: CreateNotificationCommand): Promise<any> {
    const notificationDomain = CreateNotificationCommand.fromCommand(command);
    if (command.type) {
      const totalAccounts =
        await this.accountRepository.getAllCountForNotification(
          command.type,
          false,
        );
      if (totalAccounts > 1000) {
        const batchSize = 1000;
        let skip = 0;
        let offset = 1000;
        while (true) {
          if (skip >= totalAccounts) {
            break;
          }
          const users = await this.accountRepository.getThousandForNotification(
            command.type,
            false,
            skip,
            offset,
          );
          for (const user of users) {
            const account = await this.accountRepository.getByAccountId(
              user.id,
            );
            this.appService.sendNotification(account.fcmId, command);
            skip++;
          }
        }
        const notification = await this.notificationRepository.insert(
          notificationDomain,
        );
        return notification;
      } else {
        const users = await this.accountRepository.getAllForNotification(
          command.type,
          false,
        );
        for (const user of users) {
          const account = await this.accountRepository.getByAccountId(user.id);
          this.appService.sendNotification(account.fcmId, command);
        }
        const notification = await this.notificationRepository.insert(
          notificationDomain,
        );
        return notification;
      }
    } else {
      const account = await this.accountRepository.getByAccountId(
        command.receiver,
      );
      this.appService.sendNotification(account.fcmId, command);
      notificationDomain.receiver = command.receiver;
      const notification = await this.notificationRepository.insert(
        notificationDomain,
      );
      return NotificationResponse.fromDomain(notification);
    }
  }
  // @OnEvent('create.notification')
  // async createNotification(command: CreateNotificationCommand): Promise<any> {
  //   const notificationDomain = CreateNotificationCommand.fromCommand(command);
  //   if (command.type) {
  //     let totalAccounts = 0;
  //     let users: any;
  //     if (command.type === CredentialType.Parent) {
  //       totalAccounts =
  //         await this.parentRepository.getCountByStatusNotification(
  //           command.status,
  //           false,
  //         );
  //     } else {
  //       totalAccounts = await this.accountRepository.getAllCountForNotification(
  //         command.type,
  //         false,
  //       );
  //     }
  //     if (totalAccounts > 1000) {
  //       const batchSize = 1000;
  //       let skip = 0;
  //       let offset = 1000;
  //       while (true) {
  //         if (skip >= totalAccounts) {
  //           break;
  //         }
  //         if ((command.type === CredentialType.Parent)) {
  //           users = await this.parentRepository.getThousandForNotification(
  //             command.status,
  //             false,
  //           );
  //         } else {
  //           users = await this.accountRepository.getThousandForNotification(
  //             command.type,
  //             false,
  //             skip,
  //             offset,
  //           );
  //         }
  //         for (const user of users) {
  //           const account = await this.accountRepository.getByAccountId(
  //             user.id,
  //           );
  //           if (account && account.fcmId) {
  //             this.appService.sendNotification(account.fcmId, command);
  //           }
  //           skip++;
  //         }
  //       }
  //       const notification = await this.notificationRepository.insert(
  //         notificationDomain,
  //       );
  //       return notification;
  //     } else {
  //       if (command.type === CredentialType.Parent) {
  //         users = await this.parentRepository.getAllForNotification(
  //           command.status,
  //           false,
  //         );
  //       } else {
  //         users = await this.accountRepository.getAllForNotification(
  //           command.type,
  //           false,
  //         );
  //       }
  //       for (const user of users) {
  //         const account = await this.accountRepository.getByAccountId(user.id);
  //         if (account && account.fcmId) {
  //           this.appService.sendNotification(account.fcmId, command);
  //         }
  //       }
  //       const notification = await this.notificationRepository.insert(
  //         notificationDomain,
  //       );
  //       return notification;
  //     }
  //   } else {
  //     const account = await this.accountRepository.getByAccountId(
  //       command.receiver,
  //     );
  //     this.appService.sendNotification(account.fcmId, command);
  //     notificationDomain.receiver = command.receiver;
  //     const notification = await this.notificationRepository.insert(
  //       notificationDomain,
  //     );
  //     return NotificationResponse.fromDomain(notification);
  //   }
  // }
  async updateNotification(
    command: UpdateNotificationCommand,
  ): Promise<NotificationResponse> {
    const notificationDomain = await this.notificationRepository.getById(
      command.id,
    );
    if (!notificationDomain) {
      throw new NotFoundException(
        `Notification not found with id ${command.id}`,
      );
    }
    notificationDomain.title = command.title;
    notificationDomain.body = command.body;
    notificationDomain.receiver = command.receiver;
    notificationDomain.type = command.type;
    notificationDomain.status = command.status;
    const notification = await this.notificationRepository.update(
      notificationDomain,
    );
    return NotificationResponse.fromDomain(notification);
  }
  async archiveNotification(
    command: ArchiveNotificationCommand,
  ): Promise<NotificationResponse> {
    const notificationDomain = await this.notificationRepository.getById(
      command.id,
    );
    if (!notificationDomain) {
      throw new NotFoundException(
        `Notification not found with id ${command.id}`,
      );
    }
    notificationDomain.deletedAt = new Date();
    notificationDomain.deletedBy = command?.currentUser?.id;
    notificationDomain.archiveReason = command.reason;
    const result = await this.notificationRepository.update(notificationDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.NOTIFICATION,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return NotificationResponse.fromDomain(result);
  }
  async restoreNotification(id: string): Promise<NotificationResponse> {
    const notificationDomain = await this.notificationRepository.getById(
      id,
      true,
    );
    if (!notificationDomain) {
      throw new NotFoundException(`Notification not found with id ${id}`);
    }
    const r = await this.notificationRepository.restore(id);
    if (r) {
      notificationDomain.deletedAt = null;
    }
    return NotificationResponse.fromDomain(notificationDomain);
  }
  async deleteNotification(id: string): Promise<boolean> {
    const notificationDomain = await this.notificationRepository.getById(
      id,
      true,
    );
    if (!notificationDomain) {
      throw new NotFoundException(`Notification not found with id ${id}`);
    }
    return await this.notificationRepository.delete(id);
  }
  async changeNotificationStatus(id: string): Promise<boolean> {
    const notificationDomain = await this.notificationRepository.getAll(
      id,
      false,
    );
    if (!notificationDomain) {
      throw new NotFoundException(
        `Notification not found with receiver Id ${id}`,
      );
    }
    for (const notification of notificationDomain) {
      notification.isSeen = true;
      const result = await this.notificationRepository.update(notification);
      if (!result) {
        return false;
      }
    }
    return true;
  }
}
