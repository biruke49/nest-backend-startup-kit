import { Level } from '@classification/domains/categories/level';
import { CommonEntity } from '@libs/common/common.entity';
import { CategoryStatus } from '@libs/common/enums';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('categories')
export class CategoryEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ unique: true })
  name: string;
  @Column({ nullable: true })
  description: string;
}
