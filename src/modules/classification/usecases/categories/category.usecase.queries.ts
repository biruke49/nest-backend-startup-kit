import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { Repository } from 'typeorm';
import { CategoryResponse } from './category.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
@Injectable()
export class CategoryQuery {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}
  async getCategory(
    id: string,
    withDeleted = false,
  ): Promise<CategoryResponse> {
    const category = await this.categoryRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!category[0]) {
      throw new NotFoundException(`Category not found with id ${id}`);
    }
    return CategoryResponse.fromEntity(category[0]);
  }
  async getCategories(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CategoryResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CategoryEntity>(
      this.categoryRepository,
      query,
    );
    const d = new DataResponseFormat<CategoryResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CategoryResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedCategories(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CategoryResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CategoryEntity>(
      this.categoryRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CategoryResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CategoryResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
