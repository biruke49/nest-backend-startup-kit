import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';
import { Category } from '@classification/domains/categories/category';
import { Level } from '@classification/domains/categories/level';
import { CategoryStatus } from '@libs/common/enums';
export class CreateCategoryCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  description: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateCategoryCommand): Category {
    const categoryDomain = new Category();
    categoryDomain.name = command.name;
    categoryDomain.description = command.description;
    return categoryDomain;
  }
}
export class UpdateCategoryCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  description: string;
  currentUser: UserInfo;
}
export class ArchiveCategoryCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
