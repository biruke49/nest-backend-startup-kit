import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import {
  ArchiveCategoryCommand,
  CreateCategoryCommand,
  UpdateCategoryCommand,
} from '@classification/usecases/categories/category.commands';
import { CategoryResponse } from '@classification/usecases/categories/category.response';
import { CategoryCommands } from '@classification/usecases/categories/category.usecase.commands';
import { CategoryQuery } from '@classification/usecases/categories/category.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('categories')
@ApiTags('categories')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class CategoriesController {
  constructor(
    private command: CategoryCommands,
    private categoryQuery: CategoryQuery,
  ) {}
  @Get('get-category/:id')
  @ApiOkResponse({ type: CategoryResponse })
  async getCategory(@Param('id') id: string) {
    return this.categoryQuery.getCategory(id);
  }
  @Get('get-archived-category/:id')
  @ApiOkResponse({ type: CategoryResponse })
  async getArchivedCategory(@Param('id') id: string) {
    return this.categoryQuery.getCategory(id, true);
  }
  @Get('get-categories')
  @ApiPaginatedResponse(CategoryResponse)
  async getCategories(@Query() query: CollectionQuery) {
    return this.categoryQuery.getCategories(query);
  }
  @Post('create-category')
  @UseGuards(PermissionsGuard('manage-categories'))
  @ApiOkResponse({ type: CategoryResponse })
  async createCategory(
    @CurrentUser() user: UserInfo,
    @Body() createCategoryCommand: CreateCategoryCommand,
  ) {
    createCategoryCommand.currentUser = user;
    return this.command.createCategory(createCategoryCommand);
  }
  @Put('update-category')
  @UseGuards(PermissionsGuard('manage-categories'))
  @ApiOkResponse({ type: CategoryResponse })
  async updateCategory(
    @CurrentUser() user: UserInfo,
    @Body() updateCategoryCommand: UpdateCategoryCommand,
  ) {
    updateCategoryCommand.currentUser = user;
    return this.command.updateCategory(updateCategoryCommand);
  }
  @Delete('archive-category')
  @UseGuards(PermissionsGuard('manage-categories'))
  @ApiOkResponse({ type: CategoryResponse })
  async archiveCategory(
    @CurrentUser() user: UserInfo,
    @Body() command: ArchiveCategoryCommand,
  ) {
    command.currentUser = user;
    return this.command.archiveCategory(command);
  }
  @Delete('delete-category/:id')
  @UseGuards(PermissionsGuard('manage-categories'))
  @ApiOkResponse({ type: Boolean })
  async deleteCategory(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteCategory(id, user);
  }
  @Post('restore-category/:id')
  @UseGuards(PermissionsGuard('manage-categories'))
  @ApiOkResponse({ type: CategoryResponse })
  async restoreCategory(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.restoreCategory(id, user);
  }
  @Get('get-archived-categories')
  @ApiPaginatedResponse(CategoryResponse)
  async getArchivedFeedbacks(@Query() query: CollectionQuery) {
    return this.categoryQuery.getArchivedCategories(query);
  }
}
