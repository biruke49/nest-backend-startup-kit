import { CategoryStatus } from '@libs/common/enums';
import { Level } from './level';

export class Category {
  id: string;
  name: string;
  description: string;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
