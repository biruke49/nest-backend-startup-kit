import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from 'axios';

@Injectable()
export class CronService {
  @Cron(CronExpression.EVERY_DAY_AT_9PM)
  @AllowAnonymous()
  async checkPaymentExpiry() {
    const endpointUrl = `${process.env.API_URL}/api/groups/check-payment-expiry`;
    await axios.get(`${endpointUrl}`);
  }
  @Cron(CronExpression.EVERY_DAY_AT_2AM)
  @AllowAnonymous()
  async completeAssignments() {
    const endpointUrl = `${process.env.API_URL}/api/assignments/check-uncompleted-assignments`;
    await axios.get(`${endpointUrl}`);
  }
}
