export enum TransportationMode {
  Auto = 'auto',
  Minibus = 'minibus',
}
export enum TransportationTime {
  Morning = 'Morning',
  Afternoon = 'Afternoon',
  Both = 'Both',
}
export enum StartTime {
  Auto = 'now',
  NextMonth = 'nextMonth',
  NextYear = 'nextYear',
  HaveNotDecided = 'haveNotDecided',
}
export enum CategoryStatus {
  ForKabbaKids = 'Kabba_Kids',
  ForKabbaMain = 'Kabba_Main',
  Both = 'Both',
}
export enum Gender {
  Male = 'male',
  Female = 'female',
}
export enum ContactMethod {
  Phone = 'phone',
  Telegram = 'telegram',
  SMS = 'sms',
  InPerson = 'inPerson',
}
// export enum StudentAssignmentStatus {
// New = 'new',
// WaitingToBeAssigned = 'waitingToBeAssigned',
// ContractSigned = 'contractSigned',
// Disagreed = 'disagreed',
// Assigned = 'assigned',
// }
export enum CredentialType {
  Employee = 'employee',
  Owner = 'owner',
  Driver = 'driver',
  Passenger = 'passenger',
  Corporate = 'corporate',
  Parent = 'parent',
  School = 'school',
}
export enum PaymentMethod {
  Manual = 'Kabba',
  Telebirr = 'Telebirr',
  Chapa = 'Chapa',
  Bank = 'Bank',
  InternetBanking = 'Internet Banking',
  WalletTransfer = 'Wallet Transfer',
  BookingWithdraw = 'Booking Withdraw',
  CommissionWithdraw = 'Commission Withdraw',
  WalletTransferToEmployee = 'Wallet Transfer To Employee',
  WalletTransferFromCorporate = 'Wallet Transfer from Corporate',
  RefundFromEmployee = 'RefundFromEmployee',
}
export enum PaymentStatus {
  Pending = '1',
  Success = '2',
  Timeout = '3',
  Cancelled = '4',
  Failed = '5',
  Error = '-1',
}
export enum ChapaPaymentStatus {
  Pending = 'pending',
  Success = 'success',
  Timeout = 'timeout',
  Cancelled = 'cancelled',
  Failed = 'failed',
  Error = 'error',
}
export enum WalletType {
  IndividualWallet = 'IndividualWallet',
  CorporateWallet = 'CorporateWallet',
}

export enum RouteLevel {
  EASY = 'easy',
  MODERATE = 'moderate',
  DIFFICULT = 'difficult',
  EXTREME = 'extreme',
}
export enum VehicleColor {
  Silver = 'Silver',
  Gold = 'Gold',
  Burgundy = 'Burgundy',
  BlueBlack = 'Blue Black',
  White = 'White',
  Black = 'Black',
  Red = 'Red',
  Blue = 'Blue',
  Yellow = 'Yellow',
  Orange = 'Orange',
  Green = 'Green',
}
export enum DateOfWeek {
  MONDAY = 'Mon',
  TUESDAY = 'Tue',
  WEDNESDAY = 'Wed',
  THURSDAY = 'Thu',
  FRIDAY = 'Fri',
  SATURDAY = 'Sat',
  SUNDAY = 'Sun',
}
//kids

export enum ParentStatus {
  New = 'New',
  Agreed = 'Agreed',
  Declined = 'Declined',
  WaitingToBeAssigned = 'Waiting_To_Be_Assigned',
  ContractSigned = 'Contract_Signed',
  Assigned = 'Assigned',
  PartiallyAssigned = 'Partially_Assigned',
}
export enum KidStatus {
  InSchool = 'In_School',
  AtHome = 'At_Home',
  OnTheWay = 'On_The_Way',
}
export enum KidTravelStatus {
  Alone = 'Alone',
  WithSiblings = 'With_Siblings',
  WithOthers = 'With_Others',
}
export enum SchoolStatus {
  Closed = 'Closed',
  Open = 'Open',
}
export enum DriverStatus {
  OnTheWay = 'On_The_Way',
  Arrived = 'Arrived',
  ArrivedAtSchool = 'Arrived_At_School',
  ArrivedAtHome = 'Arrived_At_Home',
}
export enum DriverAssignmentStatus {
  Active = 'Active',
  Pending = 'Pending',
  Started = 'Started',
  Completed = 'Completed',
  OnTheWay = 'On_The_Way',
}
export enum GroupStatus {
  Active = 'Active',
  Inactive = 'Inactive',
}
export enum GroupAssignmentStatus {
  Active = 'Active',
  Inactive = 'Inactive',
}
export enum JobStatus {
  Open = 'Open',
  Close = 'Close',
  Assigned = 'Assigned',
}
export enum CandidateStatus {
  Active = 'Active',
  Inactive = 'Inactive',
}
export enum NumberOfSeat {
  TaxiGroup = 4,
  MiniBusGroup = 12,
  VIPGroup = 1,
}
export enum NotificationMethod {
  Notification = 'notification',
  Sms = 'sms',
  Both = 'both',
}
export enum ChapaSplitType {
  Percentage = 'percentage',
  Flat = 'flat'
}
