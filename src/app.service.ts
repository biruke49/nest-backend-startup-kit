import { Injectable } from '@nestjs/common';
import * as firebase from 'firebase-admin';
import axios from 'axios';
import { SendSmsCommand } from '@notification/usecases/notifications/notification.commands';
@Injectable()
export class AppService {
  private apiKey = process.env.GOOGLE_DISTANCE_API;
  private afroApiKey = process.env.AFRO_API_KEY; // Use environment variables for sensitive data
  private afroSenderId = process.env.AFRO_IDENTIFIER_ID;
  private afroSenderName = process.env.AFRO_SENDER_NAME;

  apiUrl = process.env.AFRO_BASE_URL;
  // async sendNotificationLegacy(
  //   token: string,
  //   notificationData: any,
  //   sender?: any,
  //   type?: any,
  // ) {
  //   if (token) {
  //     let title = '';
  //     if (sender) {
  //       title = sender.name;
  //     } else if (notificationData.title) {
  //       title = notificationData.title;
  //     }
  //     const message = {
  //       notification: {
  //         title: title,
  //         body: notificationData.body,
  //       },
  //       data: {
  //         receiverId: notificationData.receiverId,
  //         senderId: notificationData.senderId,
  //         createdAt: notificationData.createdAt,
  //         sender: sender ? sender : null,
  //         type: type ? type : null,
  //       },
  //       to: token,
  //     };
  //     try {
  //       const response = await axios.post(
  //         'https://fcm.googleapis.com/fcm/send',
  //         message,
  //         {
  //           headers: {
  //             Authorization: `key=${process.env.FIREBASE_AUTHORIZATION_KEY}`,
  //             'Content-Type': 'application/json',
  //           },
  //         },
  //       );

  //       console.log('Notification Sent', response.data);
  //     } catch (err) {
  //       console.error('Notification Not Sent', err.response);
  //     }
  //   }
  // }
  async sendNotification(
    token: string,
    notificationData: any,
    sender?: any,
    type?: any,
  ) {
    if (token) {
      let title = '';
      if (sender) {
        title = sender.name;
      } else if (notificationData.title) {
        title = notificationData.title;
      }
      const notification = {
        notification: {
          title: title,
          body: notificationData.body,
        },
        data: {
          receiverId: notificationData.receiverId
            ? notificationData.receiverId
            : '',
          senderId: notificationData.senderId ? notificationData.senderId : '',
          createdAt: notificationData.createdAt
            ? JSON.stringify(notificationData.createdAt)
            : '',
          sender: sender ? JSON.stringify(sender) : '',
          type: type ? type : '',
        },
        token: token,
      };
      firebase
        .messaging()
        .send(notification)
        .then((response) => {
          console.log('res', response);
          return response;
        })
        .catch((error) => {
          console.log('err', error);
          return error;
        });
    }
  }
  // async calculateGroupAssignmentAmount(
  //   command: CalculateGroupAssignmentPaymentCommand,
  // ): Promise<number> {
  //   let paymentAmount =
  //     (command.initialFee + command.perKiloMeterCost * command.distance) *
  //     command.numOfDays *
  //     2;
  //   if (command.transportationTime !== TransportationTime.Both) {
  //     paymentAmount = (paymentAmount * 75) / 100;
  //   }
  //   if (command.kidTravelStatus !== KidTravelStatus.WithOthers) {
  //     paymentAmount *= command.capacity;
  //   }
  //   paymentAmount += command.roadDifficultyCost;
  //   const fifteenPercent = (paymentAmount * 15) / 100;
  //   paymentAmount += fifteenPercent;
  //   return paymentAmount;
  // }
  async getDistance(origin: string, destination: string): Promise<number> {
    const apiKey = this.apiKey;
    const apiUrl = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=${apiKey}`;

    try {
      const response = await axios.get(apiUrl);
      const distance = response.data.rows[0].elements[0].distance.value;
      return distance;
    } catch (error) {
      throw new Error('Failed to get distance from API');
    }
  }
  async sendSms(command: SendSmsCommand) {
    try {
      const response = await axios.post(
        `${this.apiUrl}/send`,
        {
          from: this.afroSenderId,
          sender: this.afroSenderName,
          to: command.phone,
          message: command.message,
        },
        {
          headers: {
            Authorization: `Bearer ${this.afroApiKey}`,
          },
        },
      );
      const responseData = response.data;
      return responseData;
    } catch (error) {
      throw error;
    }
  }
}
